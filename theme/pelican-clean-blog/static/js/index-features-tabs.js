function getAnchorTarget (url) {
  return url.split('/').pop().replace('#', '')
}

function handleTabClick(event) {
  let index = getAnchorTarget(event.target.href) - 1
  toggleTabVisibility(index)
  toggleTabActive(index)
}

function toggleTabActive(index) {
  let i = index
  let allAnchors = document.querySelectorAll('.features-tabs a')
  let otherAnchors = Array.from(allAnchors).filter((elem, i) => {
    return i != index
  })
  allAnchors[index].classList.add('tab-active')
  otherAnchors.forEach((elem) => {
    elem.classList.remove('tab-active')
  })
}

function toggleTabVisibility(index) {
  let allTabs = document.querySelectorAll('.features-tabs p')
  let otherTabs = Array.from(allTabs).filter((elem, i) => {
    return i != index
  })

  // show this element
  allTabs[index].classList.add('tab-show')
  // ... and hide the others
  otherTabs.forEach((elem) => {
    elem.classList.remove('tab-show')
  })
}

// Scripts are called at the bottom of the page, so everything is loaded.
let anchors = document.querySelectorAll('a[role="tab"]')
anchors.forEach(elem => elem.addEventListener("click", handleTabClick))
document.querySelector('.features-tabs p:nth-of-type(1)').classList.add('tab-show')
