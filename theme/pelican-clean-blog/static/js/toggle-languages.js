document.getElementById('languages-switcher').addEventListener('click', (event) => {
  elem = document.getElementById('other-languages')
  // Toggle display
  if (elem.classList.contains('display-none')) {
    elem.classList.remove('display-none')
  } else {
    elem.classList.add('display-none')
  }
});

if (window.innerWidth < 768) {
  // Hide languages block to avoid navbar being too large
  language_elem = document.getElementById('languages-block')
  // Append to list of menu links
  ul_elem = document.getElementsByClassName('navbar-nav')[0]
  li_elem = document.createElement('li')
  li_elem.appendChild(language_elem)
  ul_elem.appendChild(li_elem)
  // Make icons appear as dark
  // Navbar brand...
  logo = document.getElementsByClassName('navbar-brand')[0].childNodes[1]
  new_src = logo.src.replace('yakforms_clair.svg', 'yakforms.svg')
  logo.src = new_src

  // ...and language block
  document.getElementById('languages-block').classList.add('navbar-scrolled-down')

}


if ($(window).width() < 768) {
  elem = $('#language_block').addClass('')
  // Place along with menu links
  $menu_links = $('ul.navbar-nav')
}
