Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-1
Lang: fr
Class: alt-paragraph
BTarget: ./pages/explore.html
BText: Explorer Yakforms
BIcon: plane

# $$list-ul$$ Libérez vos formulaires de Google

Yakforms est un logiciel libre qui vous permet de créer simplement et rapidement des formulaires en ligne. Que ce soit pour une enquête avancée ou un sondage rapide, Yakforms s'adapte à vos besoins.

<div markdown class='features-tabs' role="tablist">
<div>
  <a role='tab' href='#1' class='tab-active' >Créer</a>
  <a role='tab' href='#2'>Adapter</a>
  <a role='tab' href='#3'>Partager</a>
  <a role='tab' href='#4'>Analyser</a>
</div>

![Vous pouvez facilement placer des champs de formulaires par clic ou drag and drop]({static}../../../images/frontpage_tabs/01-edit.png)
**Création de formulaires simple et rapide.** Créez vos formulaires par simple glisser-déposer. Le grand nombre de champs disponibles permet de créer des formulaires couvrant un large éventail de besoins.

![De nombreux thèmes sont disponibles pour adapter vos formulaires.]({static}../../../images/frontpage_tabs/05-themes.png)
**Des formulaires qui vous ressemblent.** Yakforms vient avec de nombreux sous-thèmes que vous pouvez sélectionner lorsque vous créez votre formulaire. Il y en a pour tous les goûts.

![Partagez facilement vos formulaires via lien, ou intégrez-les sur votre site.]({static}../../../images/frontpage_tabs/03-share.png)
**Partagez vos formulaires.** Une fois votre formulaire publié, vous pourrez le partager par email ou sur les réseaux sociaux. Vous pourrez même l'embarquer sur votre site web et autoriser son clonage afin de permettre à d'autres de gagner un temps précieux.

![Procédez à une analyse rapide de vos données sur le site, ou téléchargez vos réponses au format excel.]({static}../../../images/frontpage_tabs/04-analyze.png)
**Traitez vos résultats.** Vous pourrez  aisément accéder aux réponses en ligne, les visualiser sous forme de graphiques ou les exporter en vue de traitements plus complexes.
</div>
