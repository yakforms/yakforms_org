Title: Bienvenue
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: index-par-5
Lang: fr
BTarget: ./pages/contribute.html
BText: Contribuer à Yakforms
BIcon: user-plus

# $$users$$ Contribuez à un logiciel largement utilisé

Yakforms est un logiciel libre, placé sous licence GPL 2.0. Cela implique que toute personne peut participer à son évolution, avec ou sans compétence technique.

Rejoindre la communauté Yakforms, c'est participer à l'amélioration du logiciel : faire évoluer son code, repenser son ergonomie, traduire ses interfaces ou encore documenter son utilisation.

Vous voulez en savoir plus sur le Code de Conduite adopté par la communauté ? Rejoindre nos espaces de discussion ? Découvrir le code de Yakforms ?
