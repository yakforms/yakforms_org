Title: Help
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: help
Lang: en
Weight: 4

# Get to know Yakforms

95% of questions about Yakforms usage can be found on [the FAQ](https://docs.yakforms.org/faq/).

Our [documentation](https://docs.yakforms.org/) also has details on all the Yakforms features.

# Join the community

If you have a question or want to know more about Yakforms, you can open a topic on [the forum](https://framacolibri.org/c/yakforms/54) (the forum is mostly in French, but we'll answer in English !).

# Install and manage Yakforms

If you are a Yakforms admin, here are instructions to :

* [Install Yakforms]({filename}../Install/install-en.md).
* [Configure your instance](https://framagit.org/yakforms/yakforms/-/wikis/Configuring-Yakforms).
* [Add modules to your instance](https://framagit.org/yakforms/yakforms/-/wikis/Enhancing-Yakforms).


# Contribute to Yakforms

If you wish to help building Yakforms, please refer to our [contribution guide]({filename}../Contribute/contribute-en.md).
