Title: Télécharger
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: download
Nomenu: True
Lang: fr

## Télécharger Yakforms

Vous trouverez sur [packages.yakforms.org](https://packages.yakforms.org/) les archives suivantes :

* **yakforms_distribution.zip**
  Elle contient tout ce qu'il faut pour installer Yakforms.
* **yakforms_profile.zip**
  Elle contient uniquement le profile d'installation Drupal, sans le cœur de Drupal. Elle est à utiliser uniquement si vous souhaitez installer Yakforms sur une version spécifique de Drupal (voir le paragraphe suivant).

## Installer une autre version de Drupal 7

Vous pouvez également décider d'installer Yakforms à partir d'une autre version de Drupal. Pour cela, il faut se rendre sur [le site de Drupal 7](https://www.drupal.org/project/drupal/releases), et sélectionner la version souhaitée de Drupal `7.x`.
