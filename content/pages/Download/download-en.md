Title: Download
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: download
Nomenu: True
Lang: en

## Download Yakforms

You can find the following archives on [packages.yakforms.org](https://packages.yakforms.org/) :

* **yakforms_distribution.zip**
  It contains all you need for a full Yakforms installation.
* **yakforms_profile.zip**
  This one only contains the installation profile, without the Drupal core. Only use this one if you want to install Yakforms with a specific Drupal 7 version (see next paragraph).

## Download a specific version of Drupal

For advanced use, you might need to download another version of Drupal than the one that is distributed in the Yakforms archive. You can do that by [clicking here](https://www.drupal.org/project/drupal/releases/) and finding the corresponding `7.x` version.
