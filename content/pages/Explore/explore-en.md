Title: Explore
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: explore
Lang: en
Weight: 1

# Getting to know Yakforms

The best way to get to know Yakforms better and to figure if it fits your needs is to **try it**. Pick an instance below.

<div id="instances-list">
  <table>
  <caption>List of Yakforms instances, updated on 21/09/2021</caption>
  <tr>
    <th scope="col">Instance title</th>
    <th scope="col">URL</th>
    <th scope="col">Access status</th>
  </tr>
  <tr>
    <td scope="col">Framaforms</td>
    <td scope="col"><a href="https://framaforms.org">framaforms.org</a></td>
    <td scope="col">Open to everyone</td>
  </tr>
  <tr>
    <td>sondages.sans-nuages.fr</td>
    <td><a href="https://sondage.sans-nuage.fr">sondage.sans-nuage.fr</a></td>
    <td>Registration needed (15€ / year)</td>
  </tr>
  <tr>
    <td>Formulaire FACiLe</td>
    <td><a href="https://formulaire.facil.services/">formulaire.facil.services</a></td>
    <td>Open to everyone</td>
  </tr>
</table>
</div>

If you wish to add your instance to the list above, you'll find the instructions [here](https://framagit.org/yakforms/yakforms_org#how-can-i-add-my-instance-to-this-site-).

# Having a problem ?

If you encounter a problem using Yakforms, you'll find answers on [our help page]({filename}../Help/help-en.md).

# A step further

If you still have questions, you can also look up the following links :

* [Advanced documentation »](https://docs.yakforms.org)
* [Technical documentation »](https://framagit.org/yakforms/yakforms/-/wikis/home)
* [Contribute to Yakforms »]({filename}../Contribute/contribute-en.md)
