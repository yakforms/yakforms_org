Title: Explorer
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: explore
Lang: fr
Weight: 1

# Découvrir Yakforms

Le meilleur moyen de savoir si Yakforms correspond à vos besoins, c'est de **l'essayer**. Les instances ouvertes sont listées ci-dessous :

<div id="instances-list">
  <table>
  <caption>Liste des instances Yakforms, en date du 21/09/2021</caption>
  <tr>
    <th>Nom de l'instance</th>
    <th>Lien</th>
    <th>Modalités d'accès</th>
  </tr>
  <tr>
    <td>Framaforms</td>
    <td><a href="https://framaforms.org">framaforms.org</a></td>
    <td>Ouverte à tou⋅tes</td>
  </tr>
  <tr>
    <td>sondages.sans-nuages.fr</td>
    <td><a href="https://sondage.sans-nuage.fr">sondage.sans-nuage.fr</a></td>
    <td>Sur adhésion (15€ / an)</td>
  </tr>
  <tr>
    <td>Formulaire FACiLe</td>
    <td><a href="https://formulaire.facil.services/">formulaire.facil.services</a></td>
    <td>Ouverte à tou⋅tes</td>
  </tr>
</table>
</div>

Si vous souhaitez ajouter votre instance à la liste ci-dessus, vous trouverez les instructions [ici](https://framagit.org/yakforms/yakforms_org#how-can-i-add-my-instance-to-this-site-).

# Pour aller plus loin

* [FAQ »](https://docs.yakforms.org/faq)
* [Documentation »](https://docs.yakforms.org)
* [Forum »](https://framacolibri.org/c/yakforms/)
