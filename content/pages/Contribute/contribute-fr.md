Title: Contribuer
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Slug: contribute
Lang: fr
Weight: 3

Yakforms est un **logiciel libre** ouvert aux contributions et aux suggestions.

Pour cela, la [**section dédiée du forum Framacolibri**](https://framacolibri.org/c/yakforms/54) nous permet d'échanger sur des propositions d'améliorations, de relever des bugs, de partager nos bonnes pratiques, etc.

Chacun⋅e est libre de s'y inscrire, et d'ouvrir un sujet.

# $$lightbulb-o$$ Contribuer sans coder
Voici une liste des tâches auxquelles vous pouvez vous atteler sans compétence particulière en informatique :

<div class='links-group'>
<div class='links-group-el'>
  <a href="https://weblate.framasoft.org/engage/framaforms/">
    <span>
    <i class="fa fa-language fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Traduire</div>
    <div class='links-group-description'>le logiciel avec Weblate</div>
    </span>
  </a>
</div>
<div class='links-group-el'>
  <a href="https://framagit.org/yakforms/yakforms-docs">
    <span>
    <i class="fa fa-book fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Documenter</div>
    <div class='links-group-description'>l'utilisation du logiciel</div>
    </span>
  </a>
</div>
<div class='links-group-el'>
  <a href="https://framacolibri.org/c/yakforms/">
    <span>
    <i class="fa fa-comments fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Animer</div>
    <div class='links-group-description'>les échanges sur le forum d'entraide</div>
    </span>
  </a>
</div>
<div class='links-group-el'>
  <a href="https://framacolibri.org/c/yakforms/54">
    <span>
    <i class="fa fa-hand-pointer-o fa-2x" aria-hidden="true"></i>
    </span>
    <span>
    <div class='links-group-subtitle'>Concevoir</div>
    <div class='links-group-description'>l'interface du logiciel</div>
    </span>
  </a>
</div>
</div>

Le parcours utilisateur du logiciel, en particulier, ne demande qu'à être repensé. Les options de création d'un formulaire sont nombreuses et prêtent régulièrement à confusion. Si l'UX et UI vous intéressent, pourquoi ne pas réfléchir à faire les choses autrement ?

# $$terminal$$ Contribuer au code

Si vous avez des **compétences techniques**, vous pouvez également contribuer au code de Yakforms.

Yakforms utilise toute la puissance de [Drupal 7](https://www.drupal.org/drupal-7.0/fr), le fameux <abbr title="Content Management System">CMS</abbr> utilisé massivement partout sur Internet, et intègre de nombreuses fonctionnalités de son cœur et de ses modules. Développer Yakforms, c'est contribuer aux différents modules qui le composent.

Voici les différents moyens de contribuer :

* **Travailler sur une _issue_ ouverte :** si c'est votre première contribution à Yakforms, n'hésitez pas à vous attribuer les issues labellées `🤏 easy`, qui sont parfaites pour un début. N'hésitez pas également à **participer aux discussions** en cours sous les issues pour donner votre point de vue sur les différentes options techniques.
* **Écrire des tests : ** Yakforms peut utiliser le [framework de tests fonctionnels natif de Drupal 7](https://www.drupal.org/docs/7/testing/simpletest-testing-tutorial-drupal-7). Développer des tests pourrait permettre de tester automatiquement les fonctionnalités de Yakforms avant toute action de `merge`, ce qui mènerait à un processus de développement plus rigoureux. Isoler une fonctionnalité et développer un test spécifique serait d'une grande aide.

 Le [dépôt Framagit](https://framagit.org/yakforms/yakforms) donne l'accès au code et aux gestions de tickets (_issues_). Si vous maîtrisez cet outil, vous êtes les bienvenu⋅es sur cet espace.

# $$hand-spock-o$$ Code de conduite

Afin que les espaces de contribution restent ouverts et inclusifs pour tou⋅te⋅s, nous vous demandons de respecter les quelques points suivants :

* Faire preuve de politesse et de patience.
* Refuser et signaler toute forme de discrimination, que ce soit raciale, sexiste, homophobe, validiste ou autre.
* Éviter toute condescendance de type « techbro » : ce n'est pas parce que vous avez des compétences techniques que vous avez raison, ni que vous êtes la personne la plus pertinente sur tous les sujets.

_La communauté Yakforms est naissante, elle doit encore déterminer le code de conduite qu'elle souhaite suivre. N'hésitez pas à ouvrir un sujet à ce propos sur [le forum](https://framacolibri.org/c/yakforms/54)._
