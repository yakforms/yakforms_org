Title: Mentions légales
Authors: moi
Date: 2020-06-14
Modified: 2020-06-14
Status: hidden
Slug: legals
Lang: fr

## Editeur

Le site web **yakforms.org** est géré par l’association Framasoft, association à but non lucratif régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, déclarée en sous-préfecture d’Arles le 2 décembre 2003 sous le n° 0132007842.

```
Siège social : Association Framasoft
c/o Locaux Motiv
10 bis, rue Jangot
69007 LYON
FRANCE
```

Pour contacter l'association : [Formulaire de contact](https://contact.framasoft.org).

### Direction de la publication
Association Framasoft représentée par ses co-président⋅es.

### Hébergement
```
Hetzner Online GmbH
Industriestr. 25
91710 Gunzenhausen
Germany
Tél.: +49 (0)9831 505-0
```

## Licence
Sauf mention contraire, les contenus de ce site web sont placés sous [licence CC-BY-SA version 2.0](https://creativecommons.org/licenses/by-sa/2.0/) ou ultérieure.

## Données personnelles et cookies
Très attentif à la protection des données à caractère personnel, Framasoft s'engage à ce que la collecte et le traitement des données, effectués à partir du site **yakforms.org**, soient conformes à la loi Informatique et Libertés et au règlement général sur la protection des données (RGPD).

Analyse statistique et confidentialité
En vue d’adapter **yakforms.org** aux demandes de nos visiteurs, nous en analysons le trafic avec le logiciel libre Matomo. Matomo génère un cookie avec un identifiant unique, dont la durée de conservation est limitée à 13 mois. Les données recueillies (adresse IP, User-Agent…) sont anonymisées et conservées pour une durée de 6 mois. Elles ne sont pas cédées à des tiers ni utilisées à d’autres fins.

Pour refuser la collecte de données :

<iframe
    title="Matomo"
    style="border: 0; height: 200px; max-width: 600px; width: 85%;"
    src="https://stats.framasoft.org/?module=CoreAdminHome&action=optOut&language=fr"
></iframe>

## Rectification des informations nominatives collectées
Conformément aux dispositions de l’article 34 de la loi n° 48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des données nominatives collectées le concernant. Pour ce faire, l’utilisateur envoie à Framasoft :

* un courrier électronique en utilisant le [formulaire de contact](https://contact.framasoft.org).
* un courrier à l’adresse du siège de l’association (indiquée ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonnées physiques et/ou électroniques, ainsi que le cas échéant la référence dont il disposerait en tant qu’utilisateur du site Framasoft.
* La modification interviendra dans des délais raisonnables à compter de la réception de la demande de l’utilisateur.

## Limitation de responsabilité
Les informations et/ou documents disponibles sur ce site sont susceptibles d’être modifiés à tout moment, et peuvent faire l’objet de mises à jour.

Ce site comporte des informations mises à disposition par des communautés ou sociétés externes ou des liens hypertextes vers d’autres sites qui n’ont pas été développés par Framasoft. Le contenu mis à disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site ne constitue pas une validation de ce site ou de son contenu. Il appartient à l’internaute d’utiliser ces informations avec discernement et esprit critique. La responsabilité de Framasoft ne saurait être engagée du fait des informations, opinions et recommandations formulées par des tiers.

Framasoft ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.

Framasoft ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.
