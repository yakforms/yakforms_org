Title: Forget Framaforms, make some room for Yakforms !
Authors: me
Date: 2021-05-25
Modified: 2020-05-25
Slug: make-room-for-yakforms
Category: News
Summary: After five years, Framaforms evolved into Yakforms !
Lang: en

In 2016, French non-profit [Framasoft](https://framasoft.org/en/) launched Framaforms, an alternative web service to Google Forms. The eponymous software was free and open-source, placed under **AGPLv2 license**, and was part of the [De-google-ify Internet](https://degooglisons-internet.org/en/) campaign which aimed to prove there are ethical alternatives to centralized GAFAM services.

Having maintained the software for 5 years, Framasoft decided to give it back to its (extensive) community. And that's that : Framaforms grew out some horns, let its hair grow long and has now become Yakforms. Let's welcome it warmly !

Read the entire story on [the Framablog »](https://framablog.org/2021/05/25/forget-about-framaforms-the-software-make-room-for-yakforms/).
